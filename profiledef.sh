#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="Liya"
iso_label="$iso_name-$iso_version"
iso_publisher="Liya <https://liyalinux.gitlab.io/>"
iso_application="Liya Installer"
iso_version="Openbox"
install_dir="liso"
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'zstd' '-Xcompression-level' '15' '-b' '1M')
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/root"]="0:0:750"
  ["/root/.automated_script.sh"]="0:0:755"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/Installation_guide"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/usr/local/bin/nvidia-remove"]="0:0:755"
  ["/usr/bin/update-initramfs"]="0:0:755"
  ["/usr/bin/update-grub"]="0:0:755"
  ["/usr/bin/timeshift-autosnap"]="0:0:755"
  ["/usr/bin/cala_initial"]="0:0:755"
  ["/etc/skel/.config/polybar/launch.sh"]="0:0:755"
)
